import 'dart:io';
import 'Floatable.dart';
import 'Flyable.dart';
import 'Plane.dart';
import 'Car.dart';
import 'Ship.dart';
import 'Vahicle.dart';

void main() {
  Plane pl1 = Plane("Engine Number1");
  pl1.fly();

  Car cr1 = Car("Engine Number2");
  cr1.run();

  Ship sh1 = Ship("Engine Number3");
  sh1.float();
  
  List<Plane> flyable = [pl1];
  List<Car> runable = [cr1];
  List<Ship> floatable = [sh1];
  print("================================================");
  print("Vehicle Flyable");
   for (var i = 0; i < flyable.length; i++) {
    if (flyable[i] is Plane) { 
      flyable[i].startEngine();
      flyable[i].stopEngine();
      flyable[i].raiseSpeed();
      flyable[i].applyBreak();
      flyable[i].fly();
    } else {
      runable[i].run();
      floatable[i].float();
    } 
   }
  print("================================================");
  print("Vehicle runable");
   for (var i = 0; i < runable.length; i++) {
    if (runable[i] is Car) { 
      runable[i].startEngine();
      runable[i].stopEngine();
      runable[i].raiseSpeed();
      runable[i].applyBreak();
      runable[i].run();
    } else {
      flyable[i].fly();
      floatable[i].float();
    } 
   }
  print("================================================");
  print("Vehicle Floatable");
   for (var i = 0; i < floatable.length; i++) {
    if (floatable[i] is Ship) { 
      floatable[i].startEngine();
      floatable[i].stopEngine();
      floatable[i].raiseSpeed();
      floatable[i].applyBreak();
      floatable[i].float();
    } else {
      runable[i].run();
      flyable[i].fly();
    } 
   }
  print("================================================");

  
}
