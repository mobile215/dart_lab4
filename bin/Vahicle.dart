import 'dart:io';

abstract class Vahicle {
  String name = '';
  int number = 0;

  Vahicle(String name, int number) {
    this.name = name;
    this.number = number;
  }
  String getEngine() {
    return name;
  }

  void setEngine(String name) {
    this.name = name;
  }

  int getnumber() {
    return number;
  }

  void setnumber(int number) {
    this.number = number;
  }

  
  String toString() {
    return "engine{" + "name=" + name + ", numberOf=" + number.toString() + '}';
  }

  void startEngine();
  void stopEngine();
  void raiseSpeed();
  void applyBreak();
}
