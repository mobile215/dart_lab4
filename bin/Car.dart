import 'dart:io';

import 'Runable.dart';
import 'Vahicle.dart';

class Car extends Vahicle implements Runable {
  String name = '';

  Car(String name) : super("car", 4) {
    this.name = name;
  }

  @override
  void startEngine() {
    print("Car : " + name + " can startEngine");
  }

  @override
  void stopEngine() {
    print("Car : " + name + " can stopEngine");
  }

  @override
  void raiseSpeed() {
    print("Car : " + name + " can raiseSpeed");
  }

  @override
  void applyBreak() {
    print("Car : " + name + " can applyBreak");
  }

  @override
  void run() {
    print("Car : " + name + " can run with " + number.toString() + " wheel");
  }
}
