import 'dart:io';

import 'Flyable.dart';
import 'Vahicle.dart';

class Plane extends Vahicle implements Flyable {
  String name = '';

  Plane(String name) : super("Plane", 2) {
    this.name = name;
  }

  @override
  void startEngine() {
    print("Plane : " + name + " can startEngine");
  }

  @override
  void stopEngine() {
    print("Plane : " + name + " can stopEngine");
  }

  @override
  void raiseSpeed() {
    print("Plane : " + name + " can raiseSpeed");
  }

  @override
  void applyBreak() {
    print("Plane : " + name + " can applyBreak");
  }

  @override
  void fly() {
    print("Plane : " + name + " can fly with " + number.toString() + " wing");
  }
}
