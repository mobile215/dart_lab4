import 'dart:io';

import 'Floatable.dart';
import 'Vahicle.dart';

class Ship extends Vahicle implements Floalable {
  String name = '';

  Ship(String name) : super("ship", 0) {
    this.name = name;
  }

  @override
  void startEngine() {
    print("Ship : " + name + " can startEngine");
  }

  @override
  void stopEngine() {
    print("Ship : " + name + " can stopEngine");
  }

  @override
  void raiseSpeed() {
    print("Ship : " + name + " can raiseSpeed");
  }

  @override
  void applyBreak() {
    print("Ship : " + name + " can applyBreak");
  }

  @override
  void float() {
    print("Ship : " + name + " can float");
  }
}

